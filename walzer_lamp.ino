// S/O lastminuteengineers.com
// Interface DHT11 DHT22 w/ ESP8266 NodeMCU Using Web Server


#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include "DHT.h"
#include <FastLED.h>


const char* ssid = "Iot-enjoyer";  // Enter SSID here
const char* password = "clgb7860";  //Enter Password here
unsigned long delayPeriod = 200;

ESP8266WebServer server(80);

#define DHTTYPE DHT11   // DHT 11
#define LED_PIN   D6
#define NUM_LEDS  8
CRGB leds[NUM_LEDS];
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB


uint8_t patternCounter = 0;

uint8_t DHTPin = D8; 
DHT dht(DHTPin, DHTTYPE);                
float Temperature;
float Humidity;
float Photoresistor;
void handle_OnConnect(){
Temperature = dht.readTemperature(); // Gets the values of the temperature
Humidity = dht.readHumidity(); // Gets the values of the humidity 
Photoresistor = analogRead(A0); 
  
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr +="<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr +="<title>ESP8266 Weather Report</title>\n";
  ptr +="<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr +="body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;}\n";
  ptr +="p {font-size: 24px;color: #444444;margin-bottom: 10px;}\n";
  ptr +="</style>\n";
  ptr +="</head>\n";
  ptr +="<body>\n";
  ptr +="<div id=\"webpage\">\n";
  ptr +="<h1>RGB Lamp - Walzer</h1>\n";
  
  ptr +="<p>Temperature: ";
  ptr +=(int)Temperature;
  ptr +=" stupnu celsia</p>";
  ptr +="<p>Humidity: ";
  ptr +=(int)Humidity;
  ptr +="%</p>";
  ptr +="<p>Photoresistor: ";
  ptr +=(int)Photoresistor;
  ptr +="%</p>";
  ptr +="<a href=\"/Red\"\"><button class=\"button1\">Red</button></a>";
  ptr +="<a href=\"/Green\"\"><button class=\"button1\">Green</button></a>";
  ptr +="<a href=\"/Blue\"\"><button class=\"button1\">Modra</button></a>";
  ptr +="<a href=\"/Vypnout\"\"><button class=\"button1\">OFF</button></a>";
  ptr +="<a href=\"/Random\"\"><button class=\"button1\">Into blue</button></a>";
  
  ptr +="</div>\n";
  ptr +="</body>\n";
  ptr +="</html>\n";
  server.send(200, "text/html", ptr);
}

void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}

 
void setup() {
  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<WS2812B, LED_PIN, GRB>(leds, NUM_LEDS);
  Serial.begin(115200);
  
  dht.begin();
  pinMode(DHTPin, INPUT);
                

  
  
  Serial.println("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
  delay(1000);
  Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");  
  Serial.println(WiFi.localIP());
  server.on("/", handle_OnConnect); 
  server.onNotFound(handle_NotFound);
  server.begin();
  Serial.println("HTTP server started");
  
  server.on("/Red", []() {
    leds[0] = CRGB::Red;
    FastLED.show();
    leds[1] = CRGB::Red;
    FastLED.show();
    leds[2] = CRGB::Red;
    FastLED.show();
    leds[3] = CRGB::Red;
    FastLED.show();
    leds[4] = CRGB::Red;
    FastLED.show();
    leds[5] = CRGB::Red;
    FastLED.show();
    leds[6] = CRGB::Red;
    FastLED.show();
    leds[7] = CRGB::Red;
    FastLED.show();
    leds[8] = CRGB::Red;
    FastLED.show();
    leds[9] = CRGB::Red;
    FastLED.show();
    leds[10] = CRGB::Red;
    FastLED.show();
    leds[11] = CRGB::Red;
    FastLED.show();
    leds[12] = CRGB::Red;
    FastLED.show();
    leds[13] = CRGB::Red;
    FastLED.show();
    leds[14] = CRGB::Red;
    FastLED.show();
    handle_OnConnect();
  });

 server.on("/Green", []() {
    leds[0] = CRGB::Green;
    FastLED.show();
    leds[1] = CRGB::Green;
    FastLED.show();
    leds[2] = CRGB::Green;
    FastLED.show();
    leds[3] = CRGB::Green;
    FastLED.show();
    leds[4] = CRGB::Green;
    FastLED.show();
    leds[5] = CRGB::Green;
    FastLED.show();
    leds[6] = CRGB::Green;
    FastLED.show();
    leds[7] = CRGB::Green;
    FastLED.show();
    leds[8] = CRGB::Green;
    FastLED.show();
    leds[9] = CRGB::Green;
    FastLED.show();
    leds[10] = CRGB::Green;
    FastLED.show();
    leds[11] = CRGB::Green;
    FastLED.show();
    leds[12] = CRGB::Green;
    FastLED.show();
    leds[13] = CRGB::Green;
    FastLED.show();
    leds[14] = CRGB::Green;
    FastLED.show();
     handle_OnConnect();
  });
  
  server.on("/Blue", []() {
    leds[0] = CRGB::Blue;
    FastLED.show();
    leds[1] = CRGB::Blue;
    FastLED.show();
    leds[2] = CRGB::Blue;
    FastLED.show();
    leds[3] = CRGB::Blue;
    FastLED.show();
    leds[4] = CRGB::Blue;
    FastLED.show();
    leds[5] = CRGB::Blue;
    FastLED.show();
    leds[6] = CRGB::Blue;
    FastLED.show();
    leds[7] = CRGB::Blue;
    FastLED.show();
    leds[8] = CRGB::Blue;
    FastLED.show();
    handle_OnConnect();
    });

    server.on("/Random", [] () {
      for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB::Blue;
    FastLED.show();
    delay(delayPeriod);
    leds[i] = CRGB::Black;
    leds[i] = CRGB::Blue;
    FastLED.show();
    delay(delayPeriod);
    leds[i] = CRGB::Black;
    leds[i] = CRGB::Blue;
    FastLED.show();
    delay(delayPeriod);
    leds[i] = CRGB::Black;
    leds[i] = CRGB::Blue;
    FastLED.show();
    delay(delayPeriod);
    leds[i] = CRGB::Black;
    leds[i] = CRGB::Blue;
    FastLED.show();
    delay(delayPeriod);
    leds[i] = CRGB::Black;
    leds[i] = CRGB::Blue;
    FastLED.show();
}    
  handle_OnConnect();

      });
    
    server.on("/Vypnout", []() {
    leds[0] = CRGB::Black;
    FastLED.show();
    leds[1] = CRGB::Black;
    FastLED.show();
    leds[2] = CRGB::Black;
    FastLED.show();
    leds[3] = CRGB::Black;
    FastLED.show();
    leds[4] = CRGB::Black;
    FastLED.show();
    leds[5] = CRGB::Black;
    FastLED.show();
    leds[6] = CRGB::Black;
    FastLED.show();
    leds[7] = CRGB::Black;
    FastLED.show();
    leds[8] = CRGB::Black;
    FastLED.show();
    handle_OnConnect();
  });
  server.on("/Fade", []() {
    movingDots();
    FastLED.show();
    handle_OnConnect();
  });
  
}

void movingDots() {
  
  uint16_t posBeat  = beatsin16(30, 0, NUM_LEDS - 1, 0, 0);
  uint16_t posBeat2 = beatsin16(60, 0, NUM_LEDS - 1, 0, 0);

  uint16_t posBeat3 = beatsin16(30, 0, NUM_LEDS - 1, 0, 32767);
  uint16_t posBeat4 = beatsin16(60, 0, NUM_LEDS - 1, 0, 32767);

  // Wave for LED color
  uint8_t colBeat  = beatsin8(45, 0, 255, 0, 0);

  leds[(posBeat + posBeat2) / 2]  = CHSV(colBeat, 255, 255);
  leds[(posBeat3 + posBeat4) / 2]  = CHSV(colBeat, 255, 255);

  fadeToBlackBy(leds, NUM_LEDS, 10);
}
void redWhiteBlue() {

  uint16_t sinBeat   = beatsin16(30, 0, NUM_LEDS - 1, 0, 0);
  uint16_t sinBeat2  = beatsin16(30, 0, NUM_LEDS - 1, 0, 21845);
  uint16_t sinBeat3  = beatsin16(30, 0, NUM_LEDS - 1, 0, 43690);

  leds[sinBeat]   = CRGB::Blue;
  leds[sinBeat2]  = CRGB::Red;
  leds[sinBeat3]  = CRGB::White;
  
  fadeToBlackBy(leds, NUM_LEDS, 10);
}

void loop() {
  delay(10);
  server.handleClient();
 redWhiteBlue();

}
